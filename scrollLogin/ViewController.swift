//
//  ViewController.swift
//  scrollLogin
//
//  Created by karmaa lab on 10/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var scrollV: UIScrollView!
    @IBOutlet weak var userPasstxt: UITextField!
    @IBOutlet weak var userNametxt: UITextField!
    @IBAction func doSigIn(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "homeViewController") as? homeViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userPasstxt.delegate = self
        userNametxt.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
//            scrollV.contentInsetAdjustmentBehavior = .never
//        scrollV.isScrollEnabled = false
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }
    @objc func adjustForKeyboard(notification: Notification)
    {
        // 1
        let userInfo = notification.userInfo!
        
        // 2
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        // 3
        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollV.contentInset = UIEdgeInsets.zero
        } else {
            scrollV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        scrollV.scrollIndicatorInsets = scrollV.contentInset
        
        // 4
//        let selectedRange = scrollV.selectedRange
//        scrollV.scrollRangeToVisible(selectedRange)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

            textField.resignFirstResponder()

        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        scrollV.isScrollEnabled = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
//        scrollV.isScrollEnabled = false
    }
}

