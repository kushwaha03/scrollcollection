//
//  homeViewController.swift
//  scrollLogin
//
//  Created by karmaa lab on 10/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit

class homeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    @IBOutlet weak var collectV: UICollectionView!
    var items = ["My Policies", "Insured Details", "Claim Intimation", "Ecard", "Reimbursement Claims", "Caseless Claims", "PPN Network", "Locate Us", "Network List", "Relationship Matrix", "ECS Intimation", "Ask A Query"]
    var images: [String] = [
        "1.png", "2.png", "3.png",
        "4.png", "5.png", "6.png",
        "7.png", "8.png", "9.png",
        "10.png", "2.png", "12.png",
        
        ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.backBarButtonItem?.title = ""
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
//         let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//                layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//        layout.minimumInteritemSpacing = 0
////        layout.minimumLineSpacing = 0
//        collectV!.collectionViewLayout = layout
        let cellWidth = ((UIScreen.main.bounds.width) - 32 - 30 ) / 3
        let cellLayout = collectV.collectionViewLayout as! UICollectionViewFlowLayout
        cellLayout.itemSize = CGSize(width: cellWidth, height: cellWidth)
        
    }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //      return self.items.count
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! allCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        let image = UIImage(named: images[indexPath.row])
        cell.allimg.image = image

        cell.vno.text = self.items[indexPath.item]
        cell.allbtn.tag = indexPath.row
        cell.allbtn.addTarget(self, action: #selector(self.tappedButton(sender:)), for: .touchUpInside)
        //        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        cell.cellImg.layer.borderWidth = 1
        cell.cellImg.layer.masksToBounds = false
        cell.cellImg.layer.borderColor = UIColor.white.cgColor
        cell.cellImg.backgroundColor = .white
        cell.cellImg.layer.cornerRadius = 7
        cell.cellImg.clipsToBounds = true
        
        return cell
    }
    @objc func tappedButton(sender : UIButton){
        // Your code here
        print("button tag is :",sender.tag)
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//
//        // Get profile NC
//        let profileNC = storyboard.instantiateViewController(withIdentifier: "LoginNC") as! UINavigationController
        switch sender.tag {
        case 0:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "policiViewController") as? policiViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 1:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "insuViewController") as? insuViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 2:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "claimViewController") as? claimViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 3:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EcardViewController") as? EcardViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 4:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReimViewController") as? ReimViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 5:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "caselViewController") as? caselViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 6:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ppnViewController") as? ppnViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 7:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "locatViewController") as? locatViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 8:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "netwViewController") as? netwViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 9:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "relatnViewController") as? relatnViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 10:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ecsViewController") as? ecsViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 11:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "askqViewController") as? askqViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
